package angular.technologies.lazyhero.network;

import java.util.ArrayList;

import angular.technologies.lazyhero.OrderItemDetailsSnippet;
import angular.technologies.lazyhero.OrdersSnippet;

/**
 * Created by Amud on 19/11/15.
 */
public class APIResponseModel {


    public class LoginResponseModel {

        public Boolean success;
        public int error_code;
        public String error_details;
        public String rider_id;
    }

    public class CheckInResponseModel {
        public Boolean success;
        public String error_code;
        public String error_details;


    }

    public class CheckOutResponseModel {
        public Boolean success;
        public String error_details;

        public int error;
        public String check_out_success;

    }

    public class EditProfileResponseModel {
        public Boolean success;
        int error_code;
    }

    public class GetProfileResponseModel {
        public Boolean success;
        public String error_details;

        public RiderDetails rider_details;

        public class RiderDetails {
            public String rider_id;
            public String rider_name;
            public String address;
            public String account_name;
            public String account_no;
            public String ifsc_code;
            public String license_no;
        }
    }


    public class DeliveredOrderResponseModel {
        public Boolean success;
        int error;
        public ArrayList<DeliveredOrdersSnippet> delivered_order;

        public class DeliveredOrdersSnippet {

            public String order_code;
            public String order_time;
            public Double total_amount;
            public String sp_name;
            public String sp_number;
            public String shop_address;
        }
    }

    public class OrdersDetailResponseModel {
        public Boolean success;
        public int error;
        public String error_details;

        public ArrayList<OrderItemDetailsSnippet> user_item_details;
        public ArrayList<OrderItemDetailsSnippet> confirmed_item_details;

        public OrderPaymentDetails order_payment_details;
        public OrdersSnippet.OrderDetailsSnippet orders_details;

        public class OrderPaymentDetails {
            public Double paid;
            public Double to_be_paid;
        }

    }

    public class OrderResponseModel {
        public Boolean success;
        int error;
        public ArrayList<OrdersSnippet> orders;

    }


    public class UpdateNewOrderStatusResponseModel {
        public Boolean success;
        int error;
    }

    public class RejectNewOrderResponseModel {
        public Boolean success;
        int error;
    }


    public class MarkPickedUpOrderResponseModel {
        public Boolean success;
        public String error_details;
        int error;

    }


    public class PickedUpOrderResponseModel {
        public Boolean success;
        int error;
        public ArrayList<PickedUpOrderSnippet> picked_up_order_snippet;

        public class PickedUpOrderSnippet {

            public String order_code;
            public String order_time;
            public Double total_amount;
            public String sp_name;
            public String sp_number;
            public String shop_address;
        }
    }


    public class MarkDeliveredOrderResponseModel {
        public Boolean success;
        int error;
    }


    public class AmountReceivedResponseModel {
        public Boolean success;
        int error;
    }


    public class CheckInStatusResponseModel {

        public Boolean success;
        int error;
        public int status;

    }


}
