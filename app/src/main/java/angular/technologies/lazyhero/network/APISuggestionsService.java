package angular.technologies.lazyhero.network;

import com.google.gson.annotations.SerializedName;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Ankur on 03-Aug-2015.
 */
public interface APISuggestionsService {

    @POST("/newserver/logistics/rider_login")
    void loginAPICall(@Body APIRequestModel.LoginRequestModel param,
                      Callback<APIResponseModel.LoginResponseModel> cb);


    @GET("/newserver/logistics/rider_checkin")
    void checkInToServerAPICall(@Query("rider_id") String rider_id ,
                                @Query("bike_reading") String bike_reading,
                                @Query("bike_number") String bike_number,
                                @Query("latitude") String latitude,
                                @Query("longitude") String longitude,
                                Callback<APIResponseModel.CheckInResponseModel> callback);


    @GET("/newserver/logistics/rider_details")
    void getProfileDetailsAPICall(@Query("rider_id") String rider_id,
                                  Callback<APIResponseModel.GetProfileResponseModel> cb);


    @POST("/newserver/logistics/rider_edit")
    void editProfileDetailsAPICall(@Body APIRequestModel.EditProfileRequestModel param,
                                   Callback<APIResponseModel.EditProfileResponseModel> callback);



    @GET("/newserver/logistics/rider_checkout")
    void checkOutToServerAPICall(@Query("rider_id") String rider_id ,
                                @Query("bike_reading") String bike_reading,
                                @Query("bike_number") String bike_number,
                                @Query("latitude") String latitude,
                                @Query("longitude") String longitude,
                                Callback<APIResponseModel.CheckOutResponseModel> callback);

    @GET("/newserver/logistics/get_orders_of_status")
    void getOrdersAPICall(@Query("rider_id") String m_riderId,
                             @Query("status") String m_status,
                             Callback<APIResponseModel.OrderResponseModel> cb);


    @POST("/newserver/logistics/accept_reject_order")
    void updateNewOrderStatusAPICall(@Body APIRequestModel.UpdateNewOrderStatusRequestModel param,
                                     Callback<APIResponseModel.UpdateNewOrderStatusResponseModel> cb);


    @POST("/newserver/logistics/order_picked")
    void updateToBePickOrderStatusAPICall(@Body APIRequestModel.UpdateToBePickOrderStatusRequestModel param,
                                          Callback<APIResponseModel.MarkPickedUpOrderResponseModel> cb);

    @GET("/newserver/logistics/order_details")
    void loadOrderDetailsAPICall(@Query("order_code") String m_orderCode,
                                          Callback<APIResponseModel.OrdersDetailResponseModel> cb);



    @POST("/newserver/logistics/order_delivered")
    void updatePickedOrderStatusAPICall(@Body APIRequestModel.UpdatePickedUpOrderStatusRequestModel param,
                                          Callback<APIResponseModel.MarkDeliveredOrderResponseModel> cb);

}
