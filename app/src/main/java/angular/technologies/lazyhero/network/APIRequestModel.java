package angular.technologies.lazyhero.network;

import java.util.ArrayList;

/**
 * Created by Amud on 19/11/15.
 */
public class APIRequestModel {

    public class LoginRequestModel {
        public String phone_no;
        public String password;
    }


    public class CheckInRequestModel {
        public String rider_id;
        public Double latitude;
        public Double longitude;
        public String bike_reading;
        public String bike_number;
    }

    public class CheckOutRequestModel {
        public String rider_id;
        public Double latitude;
        public Double longitude;
        public String bike_reading;
        public String bike_number;
    }

    public class EditProfileRequestModel {
        public String rider_id;
        public String address;
        public String account_name;
        public String account_no;
        public String ifsc_code;
        public String license_no;
        public String bike_number;
    }

    public class UpdateNewOrderStatusRequestModel {
      public   String rider_uid;
      public   String order_code;
      public   String action;

    }

    public class UpdateToBePickOrderStatusRequestModel {
        public   String rider_id;
        public   String order_code;

    }

    public class UpdatePickedUpOrderStatusRequestModel {
        public   String rider_id;
        public   String order_code;
        public  String amount_collected;

    }



    public class OrdersDetailRequestModel {
        public String rider_id;
        public String order_code;
    }

    public class LoadPaymentDetailsRequestModel {
        public String rider_id;
        public String order_code;
    }


    public class DeliveredOrderRequestModel {
        public String rider_id;
        public int previous_tid;
    }

    public class NewOrderRequestModel {
        public String rider_id;
        public int previous_tid;
    }


    public class AcceptNewOrderRequestModel {
        public String rider_id;
        public String order_code;
    }

    public class RejectNewOrderRequestModel {
        public String rider_id;
        public String order_code;
    }


    public class ToBePickedOrderRequestModel {
        public String rider_id;
        public int previous_tid;
    }


    public class MarkPickedUpOrderRequestModel {
        public String rider_id;
        public String order_code;
    }


    public class PickedUpOrderRequestModel {
        public String rider_id;
        public int previous_tid;
    }


    public class MarkDeliveredOrderRequestModel {
        public String rider_id;
        public String order_code;
    }

    public class AmountReceivedRequestModel {
        public String rider_id;
        public String order_code;
        public Double amount_receive;
    }

    public class CheckInStatusRequestModel {

        public int rider_id;

    }


}
