package angular.technologies.lazyhero;

/**
 * Created by Ankur on 03-Aug-2015.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import angular.technologies.lazyhero.network.APIRequestModel;
import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import angular.technologies.lazyhero.network.NetworkUtil;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class Check_in extends ActionBarActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ResultCallback<LocationSettingsResult> {

    Button btnShowLocation;


    EditText m_readingTV;
    EditText m_bikeNoTV;
    Button m_submitTV;
    SharedPreferences m_userPrefs;
    Double mLatitude;
    Double mLongitude;
    String bike_reading;
    String bike_number;
    String rider_id;
    private ProgressDialog progressDialog;
    protected GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected String TAG = "locationSetting";
    private static final String TAG2 = "LocationAddress";
    protected LocationRequest mLocationRequest;
    private SharedPreferences prefs_location;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
        else
        Log.e("buildGoogleApiClient","not available");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLatitude = 0.0d;
        mLongitude = 0.0d;
        buildLocationSettingsRequest();


        setContentView(R.layout.check_in);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        m_userPrefs = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        rider_id=m_userPrefs.getString(EditorConstants.RIDER_ID_CONSTANT,EditorConstants.RIDER_ID_DEFAULT_CONSTANT);
        m_readingTV = (EditText) findViewById(R.id.reading);
        m_bikeNoTV = (EditText) findViewById(R.id.bike_no);
        m_submitTV = (Button) findViewById(R.id.checkin);
        m_submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bike_reading = m_readingTV.getText().toString();
                bike_number = m_bikeNoTV.getText().toString();
                if (bike_reading.isEmpty() || bike_reading == null) {
                    showToast("Enter Bike Reading");
                } else if (bike_number.isEmpty() || bike_number == null) {
                    showToast("Enter Bike Number");
                } else
                    checkInToServer(bike_number, bike_reading);
            }
        });

        if (!progressDialog.isShowing())
            progressDialog.show();
        checkLocationSettings();

    }


    private void displayLocation() {

        new Handler().postDelayed((Runnable) (new Runnable() {
            public void run() {
                Log.e("displayLocation", "displayLocation");
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    mLatitude = mLastLocation.getLatitude();
                    mLongitude = mLastLocation.getLongitude();
                    Log.e("mLatitude", mLatitude + "");
                    Log.e("mLongitude", mLongitude + "");
                    if(progressDialog.isShowing())
                        progressDialog.dismiss();

                } else {
                    displayLocation();

                }


            }
        }), 1200);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(Check_in.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }




    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("error", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                displayLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(Check_in.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        displayLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );

        result.setResultCallback(this);
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e("state", "pause");

    }




    protected void getLocation() {
    }



    protected void checkInToServer(String bike_number , String bike_reading) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.CheckInRequestModel checkInRequestModel = requestModel.new CheckInRequestModel();
        checkInRequestModel.bike_number = bike_number;
        checkInRequestModel.bike_reading= bike_reading;
        checkInRequestModel.latitude=mLatitude;
        checkInRequestModel.longitude=mLongitude;
        checkInRequestModel.rider_id=rider_id;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NetworkUtil.RestApiPathUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.checkInToServerAPICall(rider_id,bike_reading,bike_number,String.valueOf(mLatitude),String.valueOf(mLongitude), new retrofit.Callback<APIResponseModel.CheckInResponseModel>() {

            @Override
            public void success(APIResponseModel.CheckInResponseModel checkInResponseModel, retrofit.client.Response response) {

                Boolean success = checkInResponseModel.success;
                if (success) {
                    SharedPreferences.Editor editor = m_userPrefs.edit();
                    editor.putInt(EditorConstants.RIDER_CHECK_LOGGED_STATUS_CONSTANT, 1);
                    editor.commit();
                    Intent intent = new Intent(Check_in.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Check_in.this.finish();

                }
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error + "retro");
                if(progressDialog.isShowing())
                    progressDialog.dismiss();

            }
        });
    }




}
