package angular.technologies.lazyhero;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import angular.technologies.lazyhero.network.APIRequestModel;
import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 21/11/15.
 */
public class NewOrdersAdapter extends BaseAdapter {


    private class ViewHolder {
        TextView orderCodeTextView;
        TextView orderDateTextView;
        TextView orderAmountTextView;
        TextView orderServProvNameTextView;
        TextView orderServProvPhoneTextView;
        TextView orderServProvAddressTextView;
        TextView orderCostumerNameTextView;
        TextView orderCostumerPhoneTextView;
        TextView orderCostumerAddressTextView;
        LinearLayout header;
        Button accept_order_button;
        Button reject_order_button;
    }

    private Activity m_activity;
    private ArrayList<OrdersSnippet> m_OrderSnippet;
    String m_riderID;

    public NewOrdersAdapter(Activity activity, ArrayList<OrdersSnippet> newOrderSnippet, String rider_id) {
        m_activity = activity;
        m_OrderSnippet = newOrderSnippet;
        m_riderID = rider_id;

    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_OrderSnippet != null) {
            count = m_OrderSnippet.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_OrderSnippet != null) {
            return m_OrderSnippet.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_OrderSnippet != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_OrderSnippet != null) {
            final int positionFinal = position;
            final ViewHolder holder;

            final String orderCode = ((OrdersSnippet) getItem(position)).order_details.order_code;
            String orderDate = ((OrdersSnippet) getItem(position)).order_details.order_time;
            final Double orderAmount = ((OrdersSnippet) getItem(position)).order_details.total_amount;
            String servProvName = ((OrdersSnippet) getItem(position)).order_details.sp_name;
            String servProvPhone = ((OrdersSnippet) getItem(position)).order_details.sp_number;
            String servProvAddress = ((OrdersSnippet) getItem(position)).order_details.shop_address;
            String costumerName = ((OrdersSnippet) getItem(position)).order_details.user_name;
            String costumerPhone = ((OrdersSnippet) getItem(position)).order_details.user_phone_number;
            String costumerAddress = ((OrdersSnippet) getItem(position)).order_details.delivery_address;


            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.new_orders_list, (ViewGroup) null);
                holder.orderCodeTextView = (TextView) convertView.findViewById(R.id.order_code);
                holder.orderDateTextView = (TextView) convertView.findViewById(R.id.order_date);
                holder.orderAmountTextView = (TextView) convertView.findViewById(R.id.order_amount);
                holder.orderServProvNameTextView = (TextView) convertView.findViewById(R.id.order_store_name);
                holder.orderServProvPhoneTextView = (TextView) convertView.findViewById(R.id.order_store_phone);
                holder.orderServProvAddressTextView = (TextView) convertView.findViewById(R.id.order_store_address);
                holder.orderCostumerNameTextView = (TextView) convertView.findViewById(R.id.costumer_name);
                holder.orderCostumerPhoneTextView = (TextView) convertView.findViewById(R.id.costumer_phone);
                holder.orderCostumerAddressTextView = (TextView) convertView.findViewById(R.id.costumer_address);


                holder.header = (LinearLayout) convertView.findViewById(R.id.head_order);
                holder.accept_order_button = (Button) convertView.findViewById(R.id.accept_order);
                holder.reject_order_button = (Button) convertView.findViewById(R.id.reject_order);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.orderCodeTextView.setText(orderCode.toString());
            holder.orderDateTextView.setText(orderDate.toString());
            holder.orderAmountTextView.setText("₹ " + Double.toString(orderAmount));
            holder.orderServProvNameTextView.setText(servProvName);
            holder.orderServProvPhoneTextView.setText(servProvPhone);
            holder.orderServProvAddressTextView.setText(servProvAddress);
            holder.orderCostumerNameTextView.setText(costumerName);
            holder.orderCostumerPhoneTextView.setText(costumerPhone);
            holder.orderCostumerAddressTextView.setText(costumerAddress);


            holder.accept_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateStatus(orderCode, "ACCEPT");
                }
            });


            holder.reject_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateStatus(orderCode, "REJECT");
                }
            });


        }
        return convertView;
    }


    private void updateStatus(final String order_code, final String action) {
        final ProgressDialog progressDialog = new ProgressDialog(m_activity);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.UpdateNewOrderStatusRequestModel updateNewOrderStatusRequestModel = requestModel.new UpdateNewOrderStatusRequestModel();
        updateNewOrderStatusRequestModel.rider_uid = m_riderID;
        updateNewOrderStatusRequestModel.order_code=order_code;
        updateNewOrderStatusRequestModel.action=action;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.updateNewOrderStatusAPICall( updateNewOrderStatusRequestModel, new retrofit.Callback<APIResponseModel.UpdateNewOrderStatusResponseModel>() {

            @Override
            public void success(APIResponseModel.UpdateNewOrderStatusResponseModel updateNewOrderStatusResponseModel, retrofit.client.Response response) {

                Boolean success = updateNewOrderStatusResponseModel.success;
                if (success) {
                    showToast("Order No. " + order_code + " " + action + "ed");

                    ((NewOrders)m_activity).getNewOrderHistory(m_riderID);

                }if(progressDialog.isShowing())
                    progressDialog.dismiss();

            }


            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                if(progressDialog.isShowing())
                    progressDialog.dismiss();

            }
        });
    }


    protected void showToast(String text) {
        Toast.makeText(m_activity, text, Toast.LENGTH_SHORT).show();
    }

}
