package angular.technologies.lazyhero;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Amud on 18/11/15.
 */
public class SplashScreen extends Activity {

    public static final String ACCOUNT = "LazyLad";
    public static final String ACCOUNT_TYPE = "AngularTechnologiesLazyLad.com";
    protected static final String TAG = "Splash-activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLoggedStatus();
        startMainActivity();
    }

    private void  checkLoggedStatus()
    {

    }

    private void startMainActivity() {

        final SharedPreferences prefs = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        prefs.edit().putBoolean(EditorConstants.APP_START_FLAG_CONSTANT, true).commit();
        new Handler().postDelayed((Runnable) (new Runnable() {

            public void run() {

                String rider_id= prefs.getString(EditorConstants.RIDER_ID_CONSTANT , EditorConstants.RIDER_ID_DEFAULT_CONSTANT);
                int check_status= prefs.getInt(EditorConstants.RIDER_CHECK_LOGGED_STATUS_CONSTANT , EditorConstants.RIDER_DEFAULT_LOGGED_CHECK_CONSTANT);

                if(rider_id.equals(EditorConstants.RIDER_ID_DEFAULT_CONSTANT))
                {
                    startActivity(new Intent(getApplicationContext(), (Login_activity.class)));
                    SplashScreen.this.finish();
                }
                else if(check_status==EditorConstants.RIDER_DEFAULT_LOGGED_CHECK_CONSTANT)
                {
                    startActivity(new Intent(getApplicationContext(), (Check_in.class)));
                    SplashScreen.this.finish();
                }
                else
                {
                    startActivity(new Intent(getApplicationContext(), (MainActivity.class)));
                    SplashScreen.this.finish();

                }
            }
        }), 3000);
    }


}
