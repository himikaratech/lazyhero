package angular.technologies.lazyhero;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import angular.technologies.lazyhero.network.APIRequestModel;
import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import angular.technologies.lazyhero.network.NetworkUtil;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 23/11/15.
 */
public class ProfileClass extends ActionBarActivity{
    TextView name_tv;
    TextView address_tv;
    TextView account_holder_name_tv;
    TextView license_no_tv;
    TextView account_number_tv;
    TextView ifsc_code_tv;
    TextView bike_no;

    Button submit;


    String name;
    String address;
    String account_holder_name;
    String license_no;
    String account_number;
    String ifsc_code;


    SharedPreferences mPrefNewOrders;
    private String m_riderID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);
        mPrefNewOrders = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        m_riderID = mPrefNewOrders.getString(EditorConstants.RIDER_ID_CONSTANT, EditorConstants.RIDER_ID_DEFAULT_CONSTANT);
        name_tv=(TextView) findViewById(R.id.rider_name);
        name_tv.setEnabled(false);
        address_tv=(TextView)findViewById(R.id.rider_address);
        account_holder_name_tv=(TextView)findViewById(R.id.account_holder_name);
        license_no_tv=(TextView)findViewById(R.id.license_no);
        account_number_tv=(TextView)findViewById(R.id.account_number);
        ifsc_code_tv=(TextView)findViewById(R.id.ifsc_code);

        submit=(Button)findViewById(R.id.submit);



        loadProfileDetails(m_riderID);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = name_tv.getText().toString();
                address = address_tv.getText().toString();
                account_holder_name = account_holder_name_tv.getText().toString();
                license_no = license_no_tv.getText().toString();
                account_number =account_number_tv.getText().toString();
                ifsc_code = ifsc_code_tv.getText().toString();

                editProfile(m_riderID);

            }
        });





    }

    private void editProfile(final String rider_id)
    {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.EditProfileRequestModel editProfileRequestModel = requestModel.new EditProfileRequestModel();
      editProfileRequestModel.rider_id=rider_id;
        editProfileRequestModel.address=address;
        editProfileRequestModel.account_name=account_holder_name;
        editProfileRequestModel.license_no=license_no;
        editProfileRequestModel.account_no=String.valueOf(account_number);
        editProfileRequestModel.ifsc_code=ifsc_code;




        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NetworkUtil.RestApiPathUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.editProfileDetailsAPICall(editProfileRequestModel, new retrofit.Callback<APIResponseModel.EditProfileResponseModel>() {

            @Override
            public void success(APIResponseModel.EditProfileResponseModel editProfileResponseModel, retrofit.client.Response response) {
                Boolean success = editProfileResponseModel.success;
                if (success) {
                    showToast("Profile Updated Successfully");
                    loadProfileDetails(rider_id);

                }
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });



    }

    private void  loadProfileDetails(String rider_id)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NetworkUtil.RestApiPathUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getProfileDetailsAPICall(rider_id, new retrofit.Callback<APIResponseModel.GetProfileResponseModel>() {

            @Override
            public void success(APIResponseModel.GetProfileResponseModel getProfileResponseModel, retrofit.client.Response response) {
                Boolean success = getProfileResponseModel.success;
               // if (success) {
                   String name= getProfileResponseModel.rider_details.rider_name;
                    String address= getProfileResponseModel.rider_details.address;
                    String account_holder_name= getProfileResponseModel.rider_details.account_name;
                    String license_no= getProfileResponseModel.rider_details.license_no;
                    String account_number= getProfileResponseModel.rider_details.account_no;
                    String ifsc_code= getProfileResponseModel.rider_details.ifsc_code;

                Log.d("account_holder_name",account_number);


                    name_tv.setText(name);
                    address_tv.setText(address);
                    account_holder_name_tv.setText(account_holder_name);
                    license_no_tv.setText(license_no);
                    account_number_tv.setText(account_number);
                    ifsc_code_tv.setText(ifsc_code);

              //  }
                if(progressDialog.isShowing())
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                if (progressDialog.isShowing())
                progressDialog.dismiss();
            }
        });


    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
