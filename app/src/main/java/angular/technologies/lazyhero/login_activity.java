package angular.technologies.lazyhero;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import angular.technologies.lazyhero.network.APIRequestModel;
import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import angular.technologies.lazyhero.network.NetworkUtil;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Ankur on 03-Aug-2015.
 */


public class Login_activity extends ActionBarActivity {
    TextView mRiderPhoneNoTV;
    TextView mRiderPassTV;
    Button mLoginB;
    String mRiderPhoneNo;
    String mRiderPass;
    SharedPreferences mPrefLogIn;
    String TAG = "login_activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hero_login);
        mPrefLogIn = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        mRiderPhoneNoTV = (TextView) findViewById(R.id.rider_phone_no);
        mRiderPassTV = (TextView) findViewById(R.id.rider_pass);
        mLoginB = (Button) findViewById(R.id.login_submit);


        mLoginB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRiderPhoneNo = mRiderPhoneNoTV.getText().toString();
                mRiderPass = mRiderPassTV.getText().toString();
                if (mRiderPhoneNo.isEmpty() || mRiderPhoneNo == null) {
                    showToast("Enter Phone No.");
                } else if (mRiderPass.isEmpty() || mRiderPass == null) {
                    showToast("Enter password");
                } else
                    loginToServer(mRiderPhoneNo, mRiderPass);
            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void loginToServer(final String user_phone_no, final String user_pass) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.LoginRequestModel loginRequestModel = requestModel.new LoginRequestModel();
        loginRequestModel.phone_no = user_phone_no;
        loginRequestModel.password = user_pass;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(NetworkUtil.RestApiPathUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        APISuggestionsService sendorderObj = restAdapter.create(APISuggestionsService.class);
        sendorderObj.loginAPICall(loginRequestModel, new Callback<APIResponseModel.LoginResponseModel>() {

            @Override
            public void success(APIResponseModel.LoginResponseModel loginResponseModel, retrofit.client.Response response) {
                boolean success = loginResponseModel.success;
                if (success) {
                    String rider_id = loginResponseModel.rider_id;
                    Log.d("rider_id", rider_id);
                    SharedPreferences.Editor editor = mPrefLogIn.edit();
                    editor.putString(EditorConstants.RIDER_ID_CONSTANT, rider_id);
                    editor.commit();
                    Intent intent = new Intent(Login_activity.this, Check_in.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Login_activity.this.finish();
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "Login_fail");
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

        });

    }
}
