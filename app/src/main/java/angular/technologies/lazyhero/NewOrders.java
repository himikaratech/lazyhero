package angular.technologies.lazyhero;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 21/11/15.
 */
public class NewOrders extends ActionBarActivity {

    Toolbar toolbar;
    private String m_riderID;

    private ListView m_newOrdersListView;
    private ArrayList<OrdersSnippet> m_ordersArraylist;
    private NewOrdersAdapter m_newOrdAdapter;
    SharedPreferences mPrefNewOrders;
    TextView m_no_itemTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.new_order_screen_layout);
        mPrefNewOrders = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        m_riderID = mPrefNewOrders.getString(EditorConstants.RIDER_ID_CONSTANT, EditorConstants.RIDER_ID_DEFAULT_CONSTANT);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        m_no_itemTV = (TextView) findViewById(R.id.no_items);


        m_newOrdersListView = (ListView) findViewById(R.id.new_orders_list_view);


        getNewOrderHistory(m_riderID);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void getNewOrderHistory(final String rider_id) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getOrdersAPICall(rider_id, "ASS", new retrofit.Callback<APIResponseModel.OrderResponseModel>() {

            @Override
            public void success(APIResponseModel.OrderResponseModel newOrderResponseModel, retrofit.client.Response response) {
                Boolean success = newOrderResponseModel.success;
                if (success) {
                    m_ordersArraylist = new ArrayList<OrdersSnippet>();
                    m_ordersArraylist = newOrderResponseModel.orders;
                    if (m_ordersArraylist.isEmpty()) {
                        m_no_itemTV.setVisibility(View.VISIBLE);
                        m_newOrdersListView.setVisibility(View.GONE);
                    } else {
                        m_newOrdAdapter = new NewOrdersAdapter(NewOrders.this, m_ordersArraylist, rider_id);
                        m_newOrdersListView.setAdapter((ListAdapter) m_newOrdAdapter);
                    }
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

