package angular.technologies.lazyhero;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

/**
 * Created by Amud on 21/11/15.
 */
public class OrderScreen extends ActionBarActivity {

        Button m_newOrdersB;
        Button m_toBePickOrdersB;
        Button m_pickedUpOrdersB;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.order_screen_layout);
            m_newOrdersB=(Button)findViewById(R.id.new_orders_B);
            m_toBePickOrdersB=(Button)findViewById(R.id.to_be_pick_order_B);
            m_pickedUpOrdersB=(Button)findViewById(R.id.picked_up_order_B);


            m_newOrdersB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), (NewOrders.class)));
                }
            });


            m_toBePickOrdersB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), (ToBePickOrders.class)));
                }
            });

            m_pickedUpOrdersB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), (PickedUpOrders.class)));
                }
            });
        }

    }


