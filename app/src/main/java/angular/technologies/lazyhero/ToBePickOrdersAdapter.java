package angular.technologies.lazyhero;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import angular.technologies.lazyhero.network.APIRequestModel;
import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 21/11/15.
 */
public class ToBePickOrdersAdapter extends BaseAdapter{



        private class ViewHolder {
            TextView orderCodeTextView;
            TextView orderDateTextView;
            TextView orderAmountTextView;
            TextView orderServProvNameTextView;
            TextView orderServProvPhoneTextView;
            TextView orderServProvAddressTextView;
            TextView orderCostumerNameTextView;
            TextView orderCostumerPhoneTextView;
            TextView orderCostumerAddressTextView;
            LinearLayout header;
            Button view_details;
            Button pick_order;
        }

        private Activity m_activity;
        private ArrayList<OrdersSnippet> m_OrdersSnippet;
        String m_riderID;

        public ToBePickOrdersAdapter(Activity activity, ArrayList<OrdersSnippet> orderSnippet, String rider_id) {
            m_activity = activity;
            m_OrdersSnippet = orderSnippet;
            m_riderID = rider_id;

        }

        @Override
        public int getCount() {
            int count = 0;
            if (m_OrdersSnippet != null) {
                count = m_OrdersSnippet.size();
            }
            return count;
        }

        @Override
        public Object getItem(int position) {
            if (m_OrdersSnippet != null) {
                return m_OrdersSnippet.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            if (m_OrdersSnippet != null) {
                return position;
            }
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (m_OrdersSnippet != null) {
                final int positionFinal = position;
                final ViewHolder holder;

                final String orderCode = ((OrdersSnippet) getItem(position)).order_details.order_code;
                String orderDate = ((OrdersSnippet) getItem(position)).order_details.order_time;
                final Double orderAmount = ((OrdersSnippet) getItem(position)).order_details.total_amount;
                String servProvName = ((OrdersSnippet) getItem(position)).order_details.sp_name;
                String servProvPhone = ((OrdersSnippet) getItem(position)).order_details.sp_number;
                String servProvAddress = ((OrdersSnippet) getItem(position)).order_details.shop_address;
                String costumerName = ((OrdersSnippet) getItem(position)).order_details.user_name;
                String costumerPhone = ((OrdersSnippet) getItem(position)).order_details.user_phone_number;
                String costumerAddress = ((OrdersSnippet) getItem(position)).order_details.delivery_address;


                if (convertView == null) {
                    holder = new ViewHolder();
                    LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = infalInflater.inflate(R.layout.to_be_pick_order_list, (ViewGroup) null);
                    holder.orderCodeTextView = (TextView) convertView.findViewById(R.id.order_code);
                    holder.orderDateTextView = (TextView) convertView.findViewById(R.id.order_date);
                    holder.orderAmountTextView = (TextView) convertView.findViewById(R.id.order_amount);
                    holder.orderServProvNameTextView = (TextView) convertView.findViewById(R.id.order_store_name);
                    holder.orderServProvPhoneTextView = (TextView) convertView.findViewById(R.id.order_store_phone);
                    holder.orderServProvAddressTextView = (TextView) convertView.findViewById(R.id.order_store_address);
                    holder.orderCostumerNameTextView = (TextView) convertView.findViewById(R.id.costumer_name);
                    holder.orderCostumerPhoneTextView = (TextView) convertView.findViewById(R.id.costumer_phone);
                    holder.orderCostumerAddressTextView = (TextView) convertView.findViewById(R.id.costumer_address);


                    holder.header = (LinearLayout) convertView.findViewById(R.id.head_order);
                    holder.view_details = (Button) convertView.findViewById(R.id.view_details);
                    holder.pick_order = (Button) convertView.findViewById(R.id.pick_order);
                    convertView.setTag(holder);

                } else {
                    holder = (ViewHolder) convertView.getTag();
                }


                holder.orderCodeTextView.setText(orderCode.toString());
                holder.orderDateTextView.setText(orderDate.toString());
                holder.orderAmountTextView.setText("₹ " + Double.toString(orderAmount));
                holder.orderServProvNameTextView.setText(servProvName);
                holder.orderServProvPhoneTextView.setText(servProvPhone);
                holder.orderServProvAddressTextView.setText(servProvAddress);
                holder.orderCostumerNameTextView.setText(costumerName);
                holder.orderCostumerPhoneTextView.setText(costumerPhone);
                holder.orderCostumerAddressTextView.setText(costumerAddress);


                holder.view_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(m_activity, OrderDetails.class);
                        intent.putExtra("order_code", orderCode);
                        m_activity.startActivity(intent);
                    }
                });


                holder.pick_order.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateStatus(orderCode, "reject");
                        ((ToBePickOrders)m_activity).getToBePickOrderHistory(m_riderID);
                    }
                });


            }
            return convertView;
        }


        private void updateStatus(final String order_code, final String action) {

            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.UpdateToBePickOrderStatusRequestModel updateToBePickOrderStatusRequestModel = requestModel.new UpdateToBePickOrderStatusRequestModel();
            updateToBePickOrderStatusRequestModel.rider_id = m_riderID;
            updateToBePickOrderStatusRequestModel.order_code=order_code;



            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://www.angulartechnologies.com")
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

            apiSuggestionsService.updateToBePickOrderStatusAPICall( updateToBePickOrderStatusRequestModel, new retrofit.Callback<APIResponseModel.MarkPickedUpOrderResponseModel>() {

                @Override
                public void success(APIResponseModel.MarkPickedUpOrderResponseModel markPickedUpOrderResponseModel, retrofit.client.Response response) {

                    Boolean success = markPickedUpOrderResponseModel.success;
                    if (success) {
                        showToast("Order No. " + order_code + " " + action + "ed");
                        ((ToBePickOrders)m_activity).getToBePickOrderHistory(m_riderID);

                    } else
                        showToast(markPickedUpOrderResponseModel.error_details);

                }


                @Override
                public void failure(RetrofitError error) {
                    Log.e("retro", error + "");

                }
            });
        }


        protected void showToast(String text) {
            Toast.makeText(m_activity, text, Toast.LENGTH_SHORT).show();
        }

    }


