package angular.technologies.lazyhero;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 21/11/15.
 */
public class OrderDetailsAdapter extends  BaseAdapter{


        private ArrayList<OrderItemDetailsSnippet> m_OrderItemDetails;
        private Activity m_activity;

        private class ViewHolder {
            TextView itemNameTextView;
            TextView itemQuantityTextView;
            TextView itemCostTextView;
            TextView itemDescTextView;
            ImageView imageView;
        }

        public OrderDetailsAdapter(Activity activity, ArrayList<OrderItemDetailsSnippet> currentOrderItemDetails) {
            m_activity = activity;
            m_OrderItemDetails = currentOrderItemDetails;
        }

        @Override
        public int getCount() {
            int count = 0;
            if (m_OrderItemDetails != null) {
                count = m_OrderItemDetails.size() ;
            }
            return count;
        }

        @Override
        public Object getItem(int position) {
                if (m_OrderItemDetails != null) {
                    return m_OrderItemDetails.get(position);
                }

            return null;
        }

        @Override
        public long getItemId(int position) {
            if (position != 0) {
                if (m_OrderItemDetails != null) {
                    return Long.parseLong(m_OrderItemDetails.get(position).item_code);
                }
            }
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

                if (m_OrderItemDetails != null  && ! m_OrderItemDetails.isEmpty()) {
                    final ViewHolder holder;
                   Log.d("size", m_OrderItemDetails.size()+"");

                    String itemCode = ((OrderItemDetailsSnippet) getItem(position)).item_code;
                    String itemName = ((OrderItemDetailsSnippet) getItem(position)).item_name;
                    String itemShortDesc = ((OrderItemDetailsSnippet) getItem(position)).item_short_desc;
                    int itemQuantity = ((OrderItemDetailsSnippet) getItem(position)).item_quantity;
                    double itemCost = ((OrderItemDetailsSnippet) getItem(position)).item_cost;
                    String itemDesc = ((OrderItemDetailsSnippet) getItem(position)).item_desc;
                    String itemImageAdd = ((OrderItemDetailsSnippet) getItem(position)).item_img_address;

                    final int pos = position;

                    if (convertView == null) {
                        holder = new ViewHolder();
                        LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = infalInflater.inflate(R.layout.order_detail_list_item, (ViewGroup) null);
                        holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                        holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                        holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                        holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                        holder.imageView = (ImageView) convertView.findViewById(R.id.current_item_image);
                        convertView.setTag(holder);
                    } else {
                        holder = (ViewHolder) convertView.getTag();
                    }

                    holder.itemNameTextView.setText(itemName);
                    holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
                    holder.itemCostTextView.setText(Double.toString(itemCost));
                    holder.itemDescTextView.setText(itemDesc);


                    Picasso.with(parent.getContext())
                            .load(itemImageAdd)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.no_image)
                            .into(holder.imageView);
                }

            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }


    }
