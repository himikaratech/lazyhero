package angular.technologies.lazyhero;

/**
 * Created by Amud on 21/11/15.
 */
public class OrdersSnippet {

    public OrderDetailsSnippet order_details;

    public class OrderDetailsSnippet {

        public String order_code;
        public String order_time;
        public Double total_amount;
        public String sp_name;
        public String sp_number;
        public String shop_address;
        public String user_name;
        public String user_phone_number;
        public String delivery_address;
    }

}
