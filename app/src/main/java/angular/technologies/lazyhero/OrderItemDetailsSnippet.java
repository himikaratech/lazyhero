package angular.technologies.lazyhero;

/**
 * Created by Amud on 21/11/15.
 */
public class OrderItemDetailsSnippet
{
    public String item_code;
    public String item_name;
    public int item_img_flag;
    public String item_img_address;
    public String item_short_desc; //Weight like 20 gm wali dabi
    public int item_quantity;
    public double item_cost;   //total cost i.e. cost*quantity
    public String item_desc;
}
