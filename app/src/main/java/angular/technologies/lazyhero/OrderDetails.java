package angular.technologies.lazyhero;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import angular.technologies.lazyhero.network.APIResponseModel;
import angular.technologies.lazyhero.network.APISuggestionsService;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 21/11/15.
 */
public class OrderDetails extends ActionBarActivity {

    Toolbar toolbar;
    SharedPreferences mPrefOrderDetails;
    private String m_orderCode;
    String m_riderID;
    private ArrayList<OrderItemDetailsSnippet> m_ordersItemDetailsArraylist;
    private OrderDetailsAdapter m_ordDetAdapter;
    private ListView m_ordersDetailsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.order_details_new);
        Intent intent = getIntent();
        m_orderCode = intent.getStringExtra("order_code");
        m_ordersItemDetailsArraylist = new ArrayList<OrderItemDetailsSnippet>();

        m_ordersDetailsListView = (ListView) findViewById(R.id.orders_details_listview);

        mPrefOrderDetails = this.getSharedPreferences(
                "angular.technologies.lazyhero", Context.MODE_PRIVATE);
        m_riderID = mPrefOrderDetails.getString(EditorConstants.RIDER_ID_CONSTANT, EditorConstants.RIDER_ID_DEFAULT_CONSTANT);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        loadOrderDetails();
    }

    private void loadOrderDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").
                        setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadOrderDetailsAPICall(m_orderCode, new retrofit.Callback<APIResponseModel.OrdersDetailResponseModel>() {

            @Override
            public void success(APIResponseModel.OrdersDetailResponseModel ordersDetailResponseModel, retrofit.client.Response response) {
                if (ordersDetailResponseModel.success) {
                    if (ordersDetailResponseModel.confirmed_item_details != null && !ordersDetailResponseModel.confirmed_item_details.isEmpty())
                    {
                        m_ordersItemDetailsArraylist = ordersDetailResponseModel.confirmed_item_details;
                        Log.d("order_detail","confirm");
                    }
                    else
                    {
                        Log.d("order_detail","user");
                        m_ordersItemDetailsArraylist = ordersDetailResponseModel.user_item_details;
                    }
                    m_ordDetAdapter = new OrderDetailsAdapter(OrderDetails.this, m_ordersItemDetailsArraylist);
                    m_ordersDetailsListView.setAdapter((ListAdapter) m_ordDetAdapter);

                } else
                    showToast(ordersDetailResponseModel.error_details);
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}

