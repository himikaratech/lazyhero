package angular.technologies.lazyhero;

/**
 * Created by Ankur on 03-Aug-2015.
 */
public class EditorConstants {

    public static String RIDER_ID_CONSTANT="RIDER_ID";
    public static String RIDER_ID_DEFAULT_CONSTANT= "";

    public static String APP_START_FLAG_CONSTANT = "APP_START_FLAG";

    public static String CITY_SELECTED_CONSTANT="CITY_SELECTED";
    public static String AREA_SELECTED_CONSTANT="AREA_SELECTED";

    public static String RIDER_PHONE_NO_CONSTANT="RIDER_PHONE_NO";

    public static String RIDER_CHECK_LOGGED_STATUS_CONSTANT="RIDER_PASS_CHECK_STATUS";
    public static int RIDER_DEFAULT_LOGGED_CHECK_CONSTANT = 0;


}
